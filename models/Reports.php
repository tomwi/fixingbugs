<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reports".
 *
 * @property integer $id
 * @property integer $attendence
 * @property integer $expanses
 */
class Reports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'attendence', 'expanses'], 'required'],
            [['id', 'attendence', 'expanses'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attendence' => 'Attendence',
            'expanses' => 'Expanses',
        ];
    }

    public function validateTitle($attribute, $params){

        $attendence = $this->$attribute;

        $connection = yii::$app->db;
        $query = "Select * from Reports where attendence = '12'";
        $attendence = $connection->createCommand($query)->queryll();

        if($attendence) {

            $this->addError("this is already exists!!");
        }
        else 
        {
            $this->addError("this is not exists!!");

        }
    }
}
