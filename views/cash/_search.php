<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cash-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'whobaught') ?>

    <?= $form->field($model, 'place') ?>

    <?= $form->field($model, 'used') ?>

    <?php // echo $form->field($model, 'invoicenum') ?>

    <?php // echo $form->field($model, 'returned') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
