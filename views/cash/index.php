<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Cash;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\widgets\DepDrop;



/* @var $this yii\web\View */
/* @var $searchModel app\models\CasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cashes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cash', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<?php
 

        $Gridcolumns = [

        'date',
        'whobaught',
        'amount',
        'place',
        'used',
        'returned',
        ];

echo ExportMenu::widget([
           'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

            'columns' => $Gridcolumns,


            ]);
            ?>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'rowOptions'=>function($model){
            if($model->returned == 'inactive')
            {
                    return ['class' => 'danger'];

            }else {

                return ['class' => 'success' ];
            }
},

        
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
             [
        'class'=>'kartik\grid\BooleanColumn',
        'attribute'=>'returned',
        'vAlign'=>'middle',
    ],



         //   'ID',
          //  'date',
            [
                'attribute' => 'date',
                'value' =>'date',
                'format'=> 'raw',
                'filter' =>DatePicker::widget([
                        'model' => $searchModel,
                        'attribute'=>'date',
                        'clientOptions' => [
                        'autoclose'=> true,
                        'format' => 'yyyy-mm-dd',
                            ]
                    ]),
                ],


           
            'whobaught',
            'place',
            'used',
            'amount',
            'invoicenum',
           //  'returned',



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

?>
</div>
