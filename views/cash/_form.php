<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\models\cash;


/* @var $this yii\web\View */
/* @var $model app\models\cash */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cash-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

   <!-- <?= $form->field($model, 'date')->textInput() ?> -->

<?= $form->field($model, 'date')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
       // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
]);?>

        <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'whobaught')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'used')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoicenum')->textInput() ?>

 <?=  $form->field($model, 'returned')->dropdownlist(['active' => 'active', 'inactive' => 'inactive']); ?> 

 <?php echo $form->field($model, 'whobaught')->textInput() ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
