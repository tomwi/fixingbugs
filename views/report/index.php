<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\chartjs\ChartJs;
use scotthuangzl\googlechart\GoogleChart;
use app\models\Reports;
use \fruppel\googlecharts\GoogleCharts;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reports-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Reports', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

        ],
    ]); ?>
<?php
   $connection = yii::$app->db;
       $rows = (new \yii\db\Query())
    ->select(['id', 'attendence'])
    ->from('reports')
    ->where(['attendence' => '12'])



    ?>


<div class="panel panel-danger">
  <div class="panel-heading">expanses</div>

  <div class="panel-body">

<!-- <h2> expanses  </h2> -->
        <?= ChartJs::widget([
    'type' => 'pie',

    'options' => [
        'height' => 60,
        'width' => 120,
        'color' => 'green',

    ],
    'data' => [
        'labels' => ["January", "February", "March", "April", "May", "June", "July"],
        'datasets' => [
            [
                'fillColor' => " #FF6384",
                'strokeColor' => "#FF6384",
                'pointColor' => "rgba(255,0,255)",
                'pointStrokeColor' => "#0000ff",
                'data' => [65, 59, 90, 81, 56, 55, 40]
            ],
            [
                'fillColor' => "rgba(255,0,0)",
                'strokeColor' => "rgba(151,187,205,1)",
                'pointColor' => "rgba(151,187,205,1)",
                'pointStrokeColor' => "#fff",
                'data' => [28, 48, 40, 19, 96, 27, 100]
            ]
        ]
    ]
]);
?>
  </div>
</div>

<div class="panel panel-success">
  <div class="panel-heading">

<!--<?php var_dump($attendence); ?>  -->
   
    <h3 class="panel-title">attendence</h3>


  </div>

  <div class="panel-body">
 <!--  <h2> attendence  </h2> -->


   <?= ChartJs::widget([
    'type' => 'bar',
    'options' => [
        'height' => 60,
        'width' => 120,
        'color' => 'green',

    ],
    'data' => [
        'labels' => ["January", "February", "March", "April", "May", "June", "July"],
        'datasets' => [
            [
                'fillColor' => " #FF6384",
                'strokeColor' => "rgba(255,0,0)",
                'pointColor' => "rgba(255,0,255)",
                'pointStrokeColor' => "#0000ff",
                'data' => [65, 59, 90, 81, 56, 55, 40]
            ],
            [
                'fillColor' => "rgba(255,0,0)",
                'strokeColor' => "rgba(151,187,205,1)",
                'pointColor' => "rgba(151,187,205,1)",
                'pointStrokeColor' => "#fff",
                'data' => [28, 48, 40, 19, 96, 27, 100]
            ]
        ]
    ]
]);
?>
        </div>

  </div>

    <div class="panel-body">
 <!--  <h2> attendence  </h2> -->

<div class="panel panel-success">
  <div class="panel-heading">


   
    <h3 class="panel-title">attendence</h3>


  </div>

  <div class="panel-body">
   

   <?php  echo GoogleChart::widget( array('visualization' => 'Gauge', 'packages' => 'gauge',
                'data' => array(
                    array('Label', 'Value'),
                    array('training', 80),
                    array('students', 55),
                    array('learnning', 68),
                ),
                'options' => array(
                    'width' => 400,
                    'height' => 120,
                    'redFrom' => 90,
                    'redTo' => 100,
                    'yellowFrom' => 75,
                    'yellowTo' => 90,
                    'minorTicks' => 5
                )
            ));
            ?>

</div>

        </div>
                </div>




    <div class="panel-body">
 <!--  <h2> attendence  </h2> -->

<div class="panel panel-success">
  <div class="panel-heading">


   
    <h3 class="panel-title">attendence</h3>


  </div>

  <div class="panel-body">
   


<?= GoogleCharts::widget([
    'id' => 'my-id',
    'visualization' => 'PieChart',
    'data' => [
        'cols' => [
            [
                'id' => 'topping',
                'label' => 'Topping',
                'type' => 'string'
            ],
            [
                'id' => 'slices',
                'label' => 'Slices',
                'type' => 'number'
            ]
        ],
        'rows' => [
            [
                'c' => [
                    ['v' => 'Mushrooms'],
                    ['v' => 3]
                ],
            ],
            [
                'c' => [
                    ['v' => 'Onions'],
                    ['v' => 1]
                ]
            ],
            [
                'c' => [
                    ['v' => 'Olives'],
                    ['v' => 1]
                ]
            ],
            [
                'c' => [
                    ['v' => 'Zucchini'],
                    ['v' => 1]
                ]
            ],
            [
                'c' => [
                    ['v' => 'Pepperoni'],
                    ['v' => 2]
                ]
            ],
        ]
    ],
    'options' => [
        'title' => 'How Much Pizza I Ate Last Night',
        'width' => 400,
        'height' => 300,
        'is3D' => true,
    ],
    'responsive' => true,
]) ?>

</div>
</div>

    <div class="panel-body">
 <!--  <h2> attendence  </h2> -->

<div class="panel panel-danger">
  <div class="panel-heading">


   
    <h3 class="panel-title">attendence</h3>


  </div>

  <div class="panel-body">



<?= GoogleCharts::widget([
        'asPng' => true,

    'visualization' => 'AreaChart',
        'options' => [
            'title' => 'Company Performance',
            'hAxis' => [
                'title' => 'Year',
                'titleTextStyle' => [
                    'color' => '#333'
                ]
            ],
            'vAxis' => [
                'minValue' => 0
            ]
        ],
    'dataArray' => [
            ['Year', 'Sales', 'Expenses'],
            ['2013',  1000,      400],
            ['2014',  1170,      460],
            ['2015',  660,       1120],
            ['2016',  1030,      540]
    ]
])
?>
</div>
</div>

</div>
