<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\personalprofile */

$this->title = 'Create Personalprofile';
$this->params['breadcrumbs'][] = ['label' => 'Personalprofiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personalprofile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
