<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\Event;
use yii\widgets\pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
     <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
     <p>
     <?= Html::button('Create Event', ['value'=>url::to('index.php?r=event/create'), 'class' => 'btn btn-success', 'id'=>'modalButton']) ?>
    </p>

 <?php   //for popup create window
     modal::begin([
         'header'=>'<h4>Event<h4>',
         'id'=>'modal',
         
         'size'=>'modal-lg',         
     ]);
     echo "<div id='modalContent'></div>";
     modal::end();   
    ?>
    <?php pjax::begin(); ?>


 
  
 <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
      'events'=> $events,
  ));

?>
</div>
