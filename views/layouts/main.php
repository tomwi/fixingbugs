<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\sidenav\SideNav;



AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<img src = "images/sky.png ">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar',
        ],



    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
        
            ['label' => 'users', 'url' => ['/user/index']],

            ['label' => 'profiles', 'url' => ['/personalprofile/index']],

            ['label' => 'reports', 'url' => ['/reports/index']],

            ['label' => 'callender', 'url' => ['/event/index']],

            ['label' => 'teams', 'url' => ['/team/index']],

                        ['label' => 'trainings status', 'url' => ['/team/index']],





                 [
            'label' => 'reports',
            'items' => [
                 ['label' => 'cash', 'url' => '/info-systems-roni/web/index.php?r=cash'],
                 '<li class="divider"></li>',
                 ['label' => 'attendence', 'url' => '/info-systems-roni/web/index.php?r=report'],
                 '<li class="divider"></li>',
                 ['label' => 'donations', 'url' => '#'],

            ],
        ],

           [
            'label' => 'contacts',
            'items' => [
                 ['label' => 'staff', 'url' => '/info-systems-roni/web/index.php?r=cash'],
                 '<li class="divider"></li>',
                 ['label' => 'children', 'url' => '/personalprofile/index'],
                 '<li class="divider"></li>',
                 ['label' => 'donators', 'url' => '#'],
                  '<li class="divider"></li>',
                 ['label' => 'partners', 'url' => '#'],


            ],
        ],

             [
                'label' => 'small cash' ,
                 'url' => ['/cash/index'] ,
                 
             //    'visible' => yii::$app->user->isGuest
             ],
                
                [
                 'label' => 'my profile' ,
                 'url' => ['/user/update/', 'id' =>  yii::$app->user->id ] ,
                 'visible' => !yii::$app->user->isGuest


                ],
           

            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);

    NavBar::end();
    ?>



    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; sky is the limit <?= date('Y') ?></p>

        <p class="pull-right">achieve more, learn more </p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
